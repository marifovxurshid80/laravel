<?php

namespace App\Http\Controllers\Post\Like;

use App\Models\Post;
use App\Models\Comment;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class StoreController extends Controller
{
    public function __invoke(Post $post)
    {
        auth()->user()->LikedPosts()->toggle($post->id);
        return redirect()->route('post.index', $post->id);
    }
}
