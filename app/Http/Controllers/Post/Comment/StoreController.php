<?php

namespace App\Http\Controllers\Post\Comment;

use App\Models\Post;
use App\Models\Comment;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Post\Comment\StoreRequest;

class StoreController extends Controller
{
    public function __invoke(StoreRequest $request,Post $post)
    {
        $id = Auth::id();
        $data = $request->validated();
        $data['user_id'] = $id;
        $data['post_id'] = $post->id;
        Comment::create($data);
        return redirect()->route('post.show', $post->id);
    }
}
