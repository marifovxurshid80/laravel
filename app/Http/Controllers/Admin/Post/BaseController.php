<?php

namespace App\Http\Controllers\Admin\Post;

use App\Http\Controllers\Controller;

class BaseController extends Controller
{
    public $service;
    public function __construct(\App\Services\PostService $service)
    {
        $this->service = $service;
    }
}
