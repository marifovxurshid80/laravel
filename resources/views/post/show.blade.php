@extends('layouts.main')
@section('content')
<main class="blog-post">
    <div class="container">
        <h1 class="edica-page-title aos-init aos-animate" data-aos="fade-up">{{ $post->title }}</h1>
        <p class="edica-blog-post-meta aos-init aos-animate" data-aos="fade-up" data-aos-delay="200">{{ $data->translatedFormat('F') }}• {{ $data->day }}, {{ $data->year }}• {{ $data->format('H:i') }} • {{ $post->comments->count() }} Comment</p>
        <section class="blog-post-featured-img aos-init aos-animate" data-aos="fade-up" data-aos-delay="300">
            <img src="{{ asset('storage/'.$post->main_image) }}" alt="featured image" class="w-100">
        </section>
        <section class="post-content">
            <div class="row">
                <div class="col-lg-9 mx-auto">
                {!! $post->content !!}
                </div>
            </div>
        </section>
        <div class="row">
            <div class="col-lg-9 mx-auto">
                <section class="py-3">
                    <form action="{{ route('post.like.store', $post->id) }}" method="POST">
                        @csrf
                        <span>{{ $post->liked_users_count }}</span>
                        <button type="submit" class="border-0 bg-transparent">
                            @auth
                            <i class="fa{{ auth()->user()->LikedPosts->contains($post->id) ? 's' : 'r' }} fa-heart"></i>
                            @endauth
                        </button>
                    </form>
                </section>
                @if ($relatedPost->count() > 0)
                <section class="related-posts">
                    <h2 class="section-title mb-4 aos-init" data-aos="fade-up">Related Posts</h2>
                    <div class="row">
                        @foreach ($relatedPost as $relatepost)
                        <div class="col-md-4 aos-init" data-aos="fade-right" data-aos-delay="100">
                            <img src="{{ asset('storage/'.$relatepost->main_image) }}" alt="related post" class="post-thumbnail">
                            <p class="post-category">{{ $relatepost->category->title }}</p>
                            <h5 class="post-title"><a href="{{ route('post.show', $relatepost->id) }}">{{ $relatepost->title }}</a></h5>
                        </div>
                        @endforeach
                    </div>
                </section>
                @endif
                <section class="comment-list">
                    <h2>Post Comments ({{ $post->comments->count() }})</h2>
                    @foreach ($post->comments as $comment)
                    <div class="comment-text mb-3 border rounded">
                        <span class="username">
                            <h6 class="">{{ $comment->user->name }}</h6>
                            <span class="text-muted float-right">{{ $comment->dateAsCarbon->diffForHumans() }}</span>
                            {{ $comment->message }}
                        </span>
                    </div>
                    @endforeach
                </section>
                @auth
                <section class="comment-section">
                    <h2 class="section-title mb-5 aos-init" data-aos="fade-up">Submit Comment</h2>
                    <form action="{{ route('post.comment.store', $post->id) }}" method="post">
                        @csrf
                        <div class="row">
                            <div class="form-group col-12 aos-init" data-aos="fade-up">
                            <label for="comment" class="sr-only">Comment</label>
                            <textarea name="message" id="comment" class="form-control" placeholder="Comment" rows="10">Comment</textarea>
                            </div>
                            <input type="hidden" name="post_id" value="{{ $post->id }}">
                        </div>
                        <div class="row">
                            <div class="col-12 aos-init" data-aos="fade-up">
                                <input type="submit" value="Send Message" class="btn btn-warning">
                            </div>
                        </div>
                    </form>
                </section>
                @endauth
            </div>
        </div>
    </div>
</main>
@endsection
