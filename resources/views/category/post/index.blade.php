@extends('layouts.main')
@section('content')
<main class="blog">
    <div class="container">
        <h1 class="edica-page-title" data-aos="fade-up">Blog</h1>
        <section class="featured-posts-section mb-5">
            <div class="row">
                @foreach ($posts as $post)
                <div class="col-md-4 fetured-post blog-post mb-5" data-aos="fade-right">
                    <div class="blog-post-thumbnail-wrapper">
                        <img src="{{ asset('storage/'.$post->preview_image) }}" alt="blog post">
                    </div>
                    <div>
                        <div class="d-flex justify-content-between">
                            <p class="blog-post-category">{{ $post->category->title }}</p>
                            @auth
                            <form action="{{ route('post.like.store', $post->id) }}" method="POST">
                                @csrf
                                <span>{{ $post->liked_users_count }}</span>
                                <button type="submit" class="border-0 bg-transparent">
                                    <i class="fa{{ auth()->user()->LikedPosts->contains($post->id) ? 's' : 'r' }} fa-heart"></i>
                                </button>
                            </form>
                            @endauth
                            @guest
                            <div class="d-flex align-items-center">
                                <div><span>{{ $post->liked_users_count }}</span></div>
                                <i class="pl-2 far fa-heart"></i>
                            </div>
                            @endguest
                        </div>
                    </div>
                    <a href="{{ route('post.show', $post->id) }}" class="blog-post-permalink">
                        <h6 class="blog-post-title">{{ $post->title }}</h6>
                    </a>
                </div>
                @endforeach
            </div>
        </section>
    </div>
</main>
@endsection
